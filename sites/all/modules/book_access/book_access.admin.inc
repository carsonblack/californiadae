<?php

/**
 * @file
 *
 * Administration interface for the Book access module.
 */

/**
 * Book access configuration page.
 *
 */
function book_access_permissions_form(&$form_state, $node) {
  $grants = array(
    'view' => array(),
    'update' => array(),
    'delete' => array(),
  );
  $grant_ids = array(
    'grant_view' => 'view',
    'grant_update' => 'update',
    'grant_delete' => 'delete',
  );

  $bid = $node->book['bid'];
  $form = array(
    '#bid' => $bid,
    '#rids' => array(),
    '#uids' => array(),
    '#theme' => array('book_access_permissions_form'),
    '#tree' => TRUE,
  );

  $roles = user_roles();

  $rids = array_keys($roles);
  $result = db_query(
    "SELECT * FROM {book_access_role} WHERE nid = %d AND rid IN (" . db_placeholders($rids) . ")",
    array_merge(array($bid), $rids)
  );

  // Build the role access permissions for the book.
  while ($book_access = db_fetch_object($result)) {
    unset($roles[$book_access->rid]);

    foreach ($grant_ids as $id => $var) {
      $grants[$var][$book_access->rid] = !empty($book_access->$id);
    }
  }

  $default_roles_access = array_filter(variable_get('book_access_default_roles_access', array()));

  // Set the default role access permissions for the roles that don't have
  // access permissions already set.
  foreach ($grant_ids as $id => $var) {
    foreach ($roles as $rid => $name) {
      $grants[$var][$rid] = !empty($default_roles_access[$var]);
    }
  }

  foreach (user_roles() as $rid => $name) {
    $form['#rids'][] = $rid;

    $form['roles']['names'][$rid] = array(
      '#value' => check_plain($name),
    );

    $form['roles']['view'][$rid] = array(
      '#type' => 'checkbox',
      '#default_value' => !empty($grants['view'][$rid]),
    );

    $form['roles']['update'][$rid] = array(
      '#type' => 'checkbox',
      '#default_value' => !empty($grants['update'][$rid]),
    );

    $form['roles']['delete'][$rid] = array(
      '#type' => 'checkbox',
      '#default_value' => !empty($grants['delete'][$rid]),
    );
  }

  $grants = array(
    'view' => array(),
    'update' => array(),
    'delete' => array(),
  );
  $uids = array();

  $result = db_query(
    "SELECT * FROM {book_access_user} bau INNER JOIN {users} u ON u.uid = bau.uid WHERE bau.nid = %d",
    $bid
  );
  while ($book_access = db_fetch_object($result)) {
    $uid = $book_access->uid;
    $uids[$uid] = $uid;

    foreach ($grant_ids as $id => $var) {
      $grants[$var][$uid] = !empty($book_access->$id);
    }
  }

  foreach ($uids as $uid) {
    $form['#uids'][] = $uid;
    $form['users']['names'][$uid] = array(
     '#value' => ($user = user_load($uid)) ? check_plain($user->name) : t('User ID %id', array('%id' => $uid))
    );

    $form['users']['view'][$uid] = array(
      '#type' => 'checkbox',
      '#default_value' => !empty($grants['view'][$uid]),
    );

    $form['users']['update'][$uid] = array(
      '#type' => 'checkbox',
      '#default_value' => !empty($grants['update'][$uid]),
    );

    $form['users']['delete'][$uid] = array(
      '#type' => 'checkbox',
      '#default_value' => !empty($grants['delete'][$uid]),
    );
  }

  $form['user']['username'] = array(
    '#type' => 'textfield',
    '#size' => 20,
    '#autocomplete_path' => 'user/autocomplete',
  );

  $grants = array_filter(variable_get('book_access_default_users_access', array()));

  $form['user']['view'] = array(
    '#type' => 'checkbox',
    '#default_value' => !empty($grants['view']),
  );

  $form['user']['update'] = array(
    '#type' => 'checkbox',
    '#default_value' => !empty($grants['update']),
  );

  $form['user']['delete'] = array(
    '#type' => 'checkbox',
    '#default_value' => !empty($grants['delete']),
  );

  $form['user']['add_user'] = array(
    '#type'  => 'submit',
    '#value' => t('Add user'),
    '#submit' => array('book_access_user_add_submit'),
  );

  $form['submit'] = array(
    '#type'  => 'submit',
    '#value' => t('Save configuration'),
  );

  return $form;
}

/**
 * Submission callback to add a new user to the permissions table.
 */
function book_access_user_add_submit($form, &$form_state) {
  $bid = $form['#bid'];
  $grants = array(
    'grant_view' => 'view',
    'grant_update' => 'update',
    'grant_delete' => 'delete',
  );
  $username = $form_state['values']['user']['username'];

  if (!($user = user_load(array('name' => $username)))) {
    drupal_set_message(
      t('The user %user does not exist.', array('%user' => $username)),
      'error'
    );
  }
  else {
    $bool = db_result(
      db_query_range(
        "SELECT 1 FROM {book_access_user} WHERE nid = %d AND uid = %d",
        $bid, $user->uid, 0, 1
      )
    );
    $row = new stdClass();
    $row->nid = $bid;
    $row->uid = $user->uid;

    foreach ($grants as $grant => $var) {
      $row->$grant = $form_state['values']['user'][$var];
    }

    drupal_write_record('book_access_user', $row, $bool ? array('nid', 'uid') : array());

    _book_access_permissions_warning();
  }

  $form_state['redirect'] = 'node/' . $bid . '/book_access';
}

/**
 * Submission callback for the administration settings.
 */
function book_access_permissions_form_submit($form, &$form_state) {
  $bid = $form['#bid'];
  $grants = array(
    'grant_view' => 'view',
    'grant_update' => 'update',
    'grant_delete' => 'delete',
  );

  $row = new stdClass();
  $row->nid = $bid;

  db_query("DELETE FROM {book_access_role} WHERE nid = %d", $bid);

  foreach ($form['#rids'] as $rid) {
    $row->rid = $rid;

    foreach ($grants as $grant => $var) {
      $row->$grant = !empty($form_state['values']['roles'][$var][$rid]);
    }

    drupal_write_record('book_access_role', $row);
  }

  foreach ($form['#uids'] as $uid) {
    $bool = db_result(
      db_query_range(
        "SELECT 1 FROM {book_access_user} WHERE nid = %d AND uid = %d",
        $bid, $uid, 0, 1
      )
    );

    $row->uid = $uid;

    foreach ($grants as $grant => $var) {
      $row->$grant = !empty($form_state['values']['users'][$var][$uid]);
    }

    drupal_write_record(
      'book_access_user', $row, $bool ? array('nid', 'uid') : array()
    );
  }

  $batch = array(
    'title' => t('Rebuilding book access permissions'),
    'operations' => array(
      array('_book_access_rebuild_batch_operation', array($bid)),
    ),
    'finished' => '_book_access_rebuild_batch_finished',
    'file' => drupal_get_path('module', 'book_access') . '/book_access.admin.inc',
  );

  batch_set($batch);
}

/**
 * Show the confirmation form to remove the permissions a user have for a
 * book.
 */
function book_access_remove_user_permissions($form_state, $node, $user) {
  $form = array(
    '#bid' => $node->book['bid'],
    '#uid' => $user->uid,
  );

  if ($top_node = node_load($node->book['bid'])) {
    $title = $top_node->title;
  }
  else {
    $title = '#' . $node->book['bid'];
  }

  return confirm_form(
    $form,
    t('Are you sure you want to remove the user %user permissions for the book %title?', array('%user' => $user->name, '%title' => $title)),
    'node/' . $node->book['bid'] . '/book_access',
     t('This action cannot be undone.'), t('Remove')
  );
}

function book_access_remove_user_permissions_submit($form, &$form_state) {
  db_query(
    "DELETE FROM {book_access_user} WHERE nid = %d AND uid = %d",
    $form['#bid'], $form['#uid']
  );

  _book_access_permissions_warning();

  $form_state['redirect'] = 'node/' . $form['#bid'] . '/book_access';
}

/**
 * Theme the permission tab added to each book.
 */
function theme_book_access_permissions_form($form) {
  $header = array();
  $rows = array();

  if (isset($form['#rids'])) {
    foreach ($form['#rids'] as $key) {
      $row = array();

      $row[] = drupal_render($form['roles']['names'][$key]);
      $row[] = drupal_render($form['roles']['view'][$key]);
      $row[] = drupal_render($form['roles']['update'][$key]);
      $row[] = drupal_render($form['roles']['delete'][$key]);
      $row[] = ' ';

      $rows[] = $row;
    }
  }

  if (isset($form['#uids'])) {
    foreach ($form['#uids'] as $key) {
      $row = array();

      $row[] = drupal_render($form['users']['names'][$key]);
      $row[] = drupal_render($form['users']['view'][$key]);
      $row[] = drupal_render($form['users']['update'][$key]);
      $row[] = drupal_render($form['users']['delete'][$key]);
      $row[] = l(t('delete'), 'book_access/delete/user_permission/' . $form['#bid'] . "/$key");

      $rows[] = $row;
    }
  }

  $rows[] = array(
    drupal_render($form['user']['username']),
    drupal_render($form['user']['view']),
    drupal_render($form['user']['update']),
    drupal_render($form['user']['delete']),
    drupal_render($form['user']['add_user']),
  );

  $header[] = t('Role/user');
  $header[] = array(
    'data' => t('View this book'),
    'class' => 'book-access-view',
  );
  $header[] = array(
    'data' => t('Edit pages'),
    'class' => 'book-access-update',
  );
  $header[] = array(
    'data' => t('Delete pages'),
    'class' => 'book-access-delete',
  );
  $header[] = t('Operations');

  return theme('table', $header, $rows) . drupal_render($form);
}

/**
 * Shows the warning message that reminds the users with the permissions to
 * edit the book permissions that they need to save the settings.
 */
function _book_access_permissions_warning() {
  drupal_set_message(t("The changes will not be saved until the <em>Save configuration</em> button is clicked."), 'warning');
}

function _book_access_rebuild_batch_operation($bid, &$context) {
  if (empty($context['sandbox'])) {
    $context['sandbox']['progress'] = 0;
    $context['sandbox']['current_node'] = 0;
    $context['sandbox']['max'] = db_result(db_query('SELECT COUNT(DISTINCT nid) FROM {book} WHERE bid = %d', $bid));
  }

  $result = db_query_range(
    "SELECT nid FROM {book} WHERE nid > %d AND bid = %d ORDER BY nid ASC",
    $context['sandbox']['current_node'], $bid, 0, 20
  );

  while ($row = db_fetch_array($result)) {
    $loaded_node = node_load($row['nid'], NULL, TRUE);

    if (!empty($loaded_node)) {
      node_access_acquire_grants($loaded_node);
    }
    $context['sandbox']['progress']++;
    $context['sandbox']['current_node'] = $row['nid'];
    $context['message'] = t('Rebuilding access permissions for %title', array('%title' => $loaded_node->title));
  }

  if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
    $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
  }
}

function _book_access_rebuild_batch_finished($success, $results, $operations) {
  if ($success) {
    drupal_set_message(t('The book access permissions have been rebuilt.'));
    node_access_needs_rebuild(FALSE);
  }
  else {
    drupal_set_message(t('The book access permissions have not been properly rebuilt.'), 'error');
  }
  cache_clear_all();
}
