#

Module author: Harry Slaughter (http://drupal.org/user/28233)
Co-maintainer: Alberto Paderno (http://drupal.org/user/55077)

This module allows access control for Drupal books on a per-book basis.

You may configure role-based view, edit and delete permissions for each
individual book. Generally, book node with no parent is referred to as a "book",
while a book node that is positioned beneath another book node is considered a
"book page".

This module is loosely based on the Forum access module.
